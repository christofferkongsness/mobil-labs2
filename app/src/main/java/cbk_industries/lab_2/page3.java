package cbk_industries.lab_2;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class page3 extends AppCompatActivity {

    private Spinner fetch, number;
    private TextView url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        setContentView(R.layout.activity_page3);

        fetch = findViewById(R.id.fetch);
        List<String> list = new ArrayList<>();
        list.add("10 min");
        list.add("1 hour");
        list.add("24 hours");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fetch.setAdapter(adapter);
        fetch.setSelection(getPreference("fetch", settings));

        fetch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("fetch", position);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        number = findViewById(R.id.number);
        List<String> list2 = new ArrayList<>();
        list2.add("10");
        list2.add("20");
        list2.add("50");
        list2.add("100");
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list2);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        number.setAdapter(adapter2);
        number.setSelection(getPreference("number", settings));

        number.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("number", position);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        url = findViewById(R.id.URL);
        url.setText(settings.getString("Url", "Enter URL"));

        url.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("Url", charSequence.toString());
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private int getPreference(String text, SharedPreferences settings)
    {
        return settings.getInt(text, 0);
    }



}
