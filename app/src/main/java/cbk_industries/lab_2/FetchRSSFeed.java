package cbk_industries.lab_2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.LogPrinter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static android.content.ContentValues.TAG;


/**
 * Created by christoffer on 01.02.2018.
 */

public class FetchRSSFeed extends AsyncTask<Void, Void, Boolean> {

    String urlText;
    public RssReader rssReader;
    Context context;
    ListView listView;
    SharedPreferences settings;

    FetchRSSFeed(String URL, RssReader rssReader, Context context, ListView listView){
        this.context = context;
        urlText = URL;

        this.rssReader = rssReader;
        this.listView = listView;
        settings = context.getSharedPreferences("UserInfo", 0);
    }
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        LogPrinter log = new LogPrinter(Log.INFO, "log: ");
        if(TextUtils.isEmpty(urlText)){
            return false;
        }
        try{
            if(!urlText.startsWith("http://") && !urlText.startsWith("https://")) {
                urlText = "http://" + urlText;
            }
            URL url = new URL(urlText);
            InputStream inputStream = url.openConnection().getInputStream();
            rssReader.getFeed(inputStream);
            return true;
        } catch (IOException e) {
            Log.e(TAG, "error", e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (success) {
            CustomAdapter ca = new CustomAdapter(context, rssReader.getInfoList(), settings.getInt("number", 20));
            listView.setAdapter(ca);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long i) {
                    RssReader.InfoBox item = (RssReader.InfoBox)adapterView.getItemAtPosition(position);
                    Intent intent = new Intent(context , page2.class);
                    intent.putExtra("url", item.link);
                    context.startActivity(intent);
                }
            });
        }
    }
}
