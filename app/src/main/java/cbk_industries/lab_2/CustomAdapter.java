package cbk_industries.lab_2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by christoffer on 02.02.2018
 */

public class CustomAdapter extends BaseAdapter implements View.OnClickListener
{

    ArrayList<RssReader.InfoBox> infoList;
    private LayoutInflater inflater;
    private Context mContext;
    int maxSize;

    public CustomAdapter(Context context, ArrayList<RssReader.InfoBox> infoBoxes, int maxSize) {
        infoList = infoBoxes;
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int [] maxNumbers = {10, 20 ,50, 100};
        this.maxSize = maxNumbers[maxSize];
    }

    @Override
    public int getCount() {
        int f = 10 > 2 ? 1 : 2;
        if(maxSize > infoList.size()) {
            return infoList.size();
        }
        else{
            return maxSize;
        }
    }

    @Override
    public Object getItem(int i) {
        return infoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        RssReader.InfoBox temp =( RssReader.InfoBox ) getItem(i);
        if(temp.imageBitMap == null) {

            View v = inflater.inflate(R.layout.listlayout, viewGroup, false);

            TextView title = v.findViewById(R.id.name);
            TextView disc = v.findViewById(R.id.description);

            title.setText(temp.title);
            disc.setText(temp.description);
            notifyDataSetChanged();
            return v;
        }
        else
        {
            View v = inflater.inflate(R.layout.listlayoutimage, viewGroup, false);

            TextView title = v.findViewById(R.id.name2);
            TextView disc = v.findViewById(R.id.description2);
            ImageView imageView = v.findViewById((R.id.image));

            title.setText(temp.title);
            disc.setText(temp.description);


            imageView.setImageBitmap(temp.imageBitMap);

            notifyDataSetChanged();

            return v;
        }
    }

    @Override
    public void onClick(View view) {

    }
}
