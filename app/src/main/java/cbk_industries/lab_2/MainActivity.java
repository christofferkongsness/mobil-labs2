package cbk_industries.lab_2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.LogPrinter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    RssReader rssReader;
    SharedPreferences settings;

    @Override
    protected void onResume() {
        super.onResume();
        fetch();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = getSharedPreferences("UserInfo", 0);

        listView = findViewById(R.id.listView);
        rssReader = new RssReader();
        fetch(findViewById(android.R.id.content));

        Timer timer = new Timer ();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run () {
                fetch();
            }
        };

// schedule the task to run starting now and then every hour...

        int[] temp = { 10, 60, 1440};
        int period = temp[settings.getInt("fetch", 0)];
        timer.schedule (hourlyTask, 0l, 1000*60*period);   // 1000*60*10 every 10 minut *60 ever hour * 1440 every 24 hours
        //timer.schedule (hourlyTask, 0l, 10000);   //testing
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch(id)
        {
            case R.id.UserPreferences:
                Intent intent = new Intent(this, page3.class);
                startActivity(intent);
        }


        return true;
    }
    //for testing only
    public void fetch(View v)
    {

        LogPrinter log = new LogPrinter(Log.INFO, "log: ");
        String url = settings.getString("Url", "No");
        if(!url.equals("No")){
           new FetchRSSFeed(url, rssReader, this, listView).execute((Void) null);
        }
    }

    public void fetch()
    {

        LogPrinter log = new LogPrinter(Log.INFO, "log: ");
        String url = settings.getString("Url", "No");
        System.out.println("url: " + url);
        if(!url.equals("No")){
            new FetchRSSFeed(url, rssReader, this, listView).execute((Void) null);
        }
    }
}
