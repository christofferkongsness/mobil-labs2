package cbk_industries.lab_2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.util.LogPrinter;
import android.util.Xml;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by christoffer on 02.02.2018.
 */

public class RssReader {

    ArrayList<InfoBox> infoList;
    String FeedTitle, FeedLink, FeedDescription;


    RssReader(){
        infoList = new ArrayList<>();
    }

    void getFeed(InputStream inputStream) {
        infoList = new ArrayList<>();
        String title = null;
        String link = null;
        String description = null;
        String imageLink = null;
        boolean isItem = false;
        //int times = 0;
        LogPrinter log = new LogPrinter(Log.INFO, "log: ");
        try {

            XmlPullParser xmlPullParser = Xml.newPullParser();

            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            xmlPullParser.setInput(inputStream, null);
            //xmlPullParser.nextTag();

            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {

                int eventType = xmlPullParser.getEventType();
                /* debug code
                if(times < 300) {
                    if (eventType == XmlPullParser.START_DOCUMENT) {
                        System.out.println("Start document");
                    } else if (eventType == XmlPullParser.END_DOCUMENT) {
                        System.out.println("End document");
                    } else if (eventType == XmlPullParser.START_TAG) {
                        System.out.println("Start tag " + xmlPullParser.getName());
                    } else if (eventType == XmlPullParser.END_TAG) {
                        System.out.println("End tag " + xmlPullParser.getName());
                    } else if (eventType == XmlPullParser.TEXT) {
                        System.out.println("Text " + xmlPullParser.getText());
                    }
                    times++;
                }
                */

                String name = xmlPullParser.getName();

                if(name == null) {
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        isItem = true;
                        title = null;
                        link = null;
                        description = null;
                        imageLink = null;
                        continue;
                    }
                }

                if (eventType == XmlPullParser.START_TAG && name.equalsIgnoreCase("title")) {
                    xmlPullParser.next();
                    title = xmlPullParser.getText();

                } else if (eventType == XmlPullParser.START_TAG && name.equalsIgnoreCase("link")) {
                    xmlPullParser.next();
                    link = xmlPullParser.getText();

                } else if (eventType == XmlPullParser.START_TAG && name.equalsIgnoreCase("description")) {
                    xmlPullParser.next();
                    description = xmlPullParser.getText();
                }
                 else if (eventType == XmlPullParser.START_TAG && name.equalsIgnoreCase("image")) {
                    xmlPullParser.next();
                    imageLink = xmlPullParser.getText();
                }
                //if (title != null && link != null && description != null) {
                if(eventType == XmlPullParser.END_TAG && name.equalsIgnoreCase("item")) {
                    if(isItem) {
                        if(title != null && title.trim().length() > 0 && link != null) {
                            InfoBox item;
                            if (imageLink == null) {
                                System.out.println("here: test");

                                item = new InfoBox(title, description, link);
                            } else {

                                item = new InfoBox(title, description, link, imageLink);
                            }
                            //log.println("text so far \nTitle: " + title + "\nLink: " + link + "\nDesc: " + description);
                            infoList.add(item);
                        }
                    }
                    else {
                        FeedTitle = title;
                        FeedLink = link;
                        FeedDescription = description;
                       // log.println("text so far \nTitle: " + title + "\nLink: " + link + "\nDesc: " + description);
                    }

                    title = null;
                    link = null;
                    description = null;
                    isItem = false;
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    ArrayList<InfoBox> getInfoList() {
        return infoList;
    }

    ArrayList<InfoBox> getFAKEInfoList() {
        infoList = new ArrayList<>();
        InfoBox item = new InfoBox("first", "desc", "link");
        infoList.add(item);
        InfoBox item2 = new InfoBox("second", "desc", "link");
        infoList.add(item2);
        item = new InfoBox("third", "desc", "link");
        infoList.add(item);
        item = new InfoBox("forth", "desc", "link");
        infoList.add(item);



        return infoList;
    }

    public class InfoBox {
       public String title;
       public String description;
       public String link;
       public Bitmap imageBitMap;

        InfoBox(String title, String description, String link){
            this.title = title;
            this.description = description;
            this.link = link;
            imageBitMap = null;
        }
        InfoBox(String title, String description, String link, String imageLink){
            this.title = title;
            this.description = description;
            this.link = link;
            System.out.println("here: "+ imageLink);
            try {
                URL newurl = new URL(imageLink);

                System.out.println("here!3");
                imageBitMap = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
                if(imageBitMap == null)
                {
                    System.out.println("nullptr");
                }
                else{
                    System.out.println("Not null");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
